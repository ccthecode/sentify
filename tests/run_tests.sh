#!/usr/bin/env bash
sleep 1
bundle install
xvfb-run --server-args="-screen 0, 1280x1024x24" bundle exec rspec
