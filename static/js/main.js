// Subnavigation Logic
var subnav = document.getElementById('subnav')
var mobileSubnav = document.getElementById('mobile-subnav')
var subnavIcon = document.querySelector('.subnav-icon')
var hasSubnav = document.querySelector('.has-subnav')
var productSubnav = document.querySelector('.product-subnav')
var timesClicked = 0;

// hasSubnav.addEventListener('mouseover', function () { toggleNavOn() })
// hasSubnav.addEventListener('mouseout', function () { toggleNavOff() })
// subnav.addEventListener('mouseover', function () { toggleNavOn() })
// subnav.addEventListener('mouseout', function () { toggleNavOff() })
// productSubnav.addEventListener('mouseover', function () { toggleNavOn() })
// productSubnav.addEventListener('mouseout', function () { toggleNavOff() })

// mobileSubnav.addEventListener('mouseover', function () { toggleNavOn() })
// mobileSubnav.addEventListener('mouseout', function () { toggleNavOff() })

function toggleNav() {
    timesClicked++;
    if (timesClicked % 2 == 0) {
        subnav.classList.add('d-none')
        mobileSubnav.classList.add('d-none')
        subnavIcon.classList.remove('flip-icon')
        productSubnav.classList.add('d-none')
    } else {
        subnav.classList.remove('d-none')
        mobileSubnav.classList.remove('d-none')
        subnavIcon.classList.add('flip-icon')
        productSubnav.classList.remove('d-none')
    }
}

function toggleNavOn() {
    timesClicked++;
    subnav.classList.remove('d-none')
    mobileSubnav.classList.remove('d-none')
    subnavIcon.classList.add('flip-icon')
    productSubnav.classList.remove('d-none')
}

function toggleNavOff() {
    timesClicked++;
    subnav.classList.add('d-none')
    mobileSubnav.classList.add('d-none')
    subnavIcon.classList.remove('flip-icon')
    productSubnav.classList.add('d-none')
}

// new Pageable("#snap", {

//     animation: 0,
//     pips: true,
//     // interval: 300,
//     delay: 0,
//     // throttle: 50,


//     events: {
//         wheel: true, // enable / disable mousewheel scrolling
//         mouse: true, // enable / disable mouse drag scrolling
//         touch: true, // enable / disable touch / swipe scrolling
//         keydown: true, // enable / disable keyboard navigation
//     },
//     // easing: function (currentTime, startPos, endPos, interval) {
//     //     // the easing function used for the scroll animation
//     //     return -endPos * (currentTime /= interval) * (currentTime - 2) + startPos;
//     // },

//     orientation: "vertical", // or 'horizontal'
//     easing: (t, b, c, d, s) => -c * (t /= d) * (t - 2) + b,
// });

// console.log("HELLO");

