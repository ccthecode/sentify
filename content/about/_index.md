---
title: About Us
descriptionone: Since 2007, Sentify has been helping organisations overcome challenges and position for future success. We have lead culture strategy and transformation initiatives in the Defence and Health sectors. We have helped delivery teams in large complex private and public environments increase throughput, reduce rework and improve quality by improving technical competency, introducing positive behaviours, becoming more transparent and aligning and prioritising activities for the greater good of the business. 
descriptiontwo: We have helped define strategy and guided digital investment decisions across not-for-profit and SMEs.  Provided cloud & application managed services in highly regulated environments. Delivering value for staff and customers, innovating, adapting and improving, walking the talk, recommending and implementing, being honest and transparent are our core values that underpin how we work and act.
ourmission: 
  title: Our Mission
  description: Enable organisations to achieve better business outcomes through improving the way they work

ourvalues:
  title: Our Values
  description: Back in 2016 we sat down as a company and captured the values we felt best represented how we worked and acted.  Today we still see these as an essential set of attributes to guide us as we continue to grow.
  list:
    - title: Deliver value for staff and customers
    - title: Innovate, adapt and improve
    - title: Walk the talk, recommend and implement
    - title: Be honest and transparent


gettingstarted:
  title: Getting to know us
  description: If you have reached this far, why not take the next step. We like to be transparent in our methods and approach and find visualising a process helps gain understanding and provide clarity.  Here is a 4 step example of what to expect with engaging with us.
  list:
    - title: Ice Breaker
      image: ice-breaker.svg
      description: Let's get to know each other.
      step: Is there a rapport and common understanding?
    - title: Drivers
      image: drivers.svg
      description: Let's clarify the outcomes you seek, what are you worried about, and how should we measure success?
      step: Do our values align?
    - title: Alignment
      image: alignment.svg
      description: Let's get together to bounce off ideas to come up with a set of prioritised actions.
      step: Are we on the same page?
    - title: Action
      image: action.svg
      description: Let's write down what each of us will do and how we will succeed together.
      step: Let's co-own the outcome.


---
