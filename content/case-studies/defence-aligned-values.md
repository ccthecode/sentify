---
weight: 1
slider_filter:
- case-study
work:
- Culture
- Agile
title: Reducing Churn and Improving Customer Satisfaction
description: The Mine Sweeper and Clearance Diving Systems Program Office in Defence had recently adopted a Kanban-driven approach to organising their work.  They had aspiration to improve the cadence of outputs to customers and fix the prioritisation issues across multiple teams and service providers.

featuredImage:
clientLogo: dod.png
modifierClass: black-logo
section:
  - title: The Problem
    description: Our client engaged Sentify to provide agile coaching, leadership coaching, technical coaching, and governance advice. The organisation was having a difficult time agreeing on multiple fronts.  Their values, purpose, communication approach all varied widely.  Consequently, the business was experiencing churn and customer dissatisfaction as a result. 
  - title: The Intervention
    description: Sentify worked with the entire office over a 2 month period to build a shared understanding of the Definition of Done, among other things.  They also facilitated the introduction of a Product Manager role and identified cultural impediments to harmony.  Finally, they a solution to successfully address siloed behaviour. Sentify reviewed the GovTeams Planner Kanban structure and quality of record-keeping to ensure that traceability and communication on requirements were effective.  This significantly improved and the overall team dynamics and established that team members would use a standardised approach to communicating issues and accomplishments. Sentify also worked with the governance team and senior stakeholders to build trust and focus on customer satisfaction as well as compliance.
  - title: The Value
    description: At the end of the eight weeks, the client's team had adopted the new procedures we had discussed.  These included the Definition of Done and a redefined and optimised their use of Planner.  They also removed a range of procedures and behaviours that did not add value to the cadence of delivery.  The allowed them to initiate the appointment of a product owner for the Minesweeping platforms. This initial experiment was followed up with a 6-month cultural improvement and team cohesion investment by senior leaders with the purpose of building on the silo-breaking efforts of team leads.  This next endeavour focussed on where long-standing issues were baked into the culture of the SPO, and many were finally resolved with the team moving forward together.
# testimonial
#testimonial:
#  by: 
#  designation: 
#  avatar:
#  text: 

# Table
#tableTitle:
#  left: 
#  right: 
#tableContent:
#  - left: 
#    right: 
---  
 
 